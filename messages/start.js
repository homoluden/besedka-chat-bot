const commands = require("../keyboard/commands");

module.exports = {
  welcomeFirst: `
Привет!

Я бот Активиста фотосообщества "Беседка".
Для начала работы укажите имя Вашего аккаунта Instagram (комманда \/${commands.setIg})

  `,
  setIg: `
Введите имя вашего аккаунта IG без символа @:
  `,
  welcomeBack: (igName) =>
  `
Привет, ${igName}!

Тебе доступны следующие команды:
  `,
  regularCommands: `
\/${commands.setIg} - вывести актуальный на текущую дату тег

\/${commands.getMyTag} - вывести актуальный на текущую дату тег

[НЕ РЕАЛИЗОВАНО] \/${commands.suspend} - временно приостановить участие в продвижении

[НЕ РЕАЛИЗОВАНО] \/${commands.resume} - вернуться к участию в продвижении

  `,
  adminCommands: `
\/${commands.addAccounts} - добавить один или несколько IG аккаунтов

\/${commands.addTags} - добавить два или болеетегов IG

[НЕ РЕАЛИЗОВАНО] \/${commands.disableAccounts} - перманентно приостановить участие аккаунтов в продвижении

[НЕ РЕАЛИЗОВАНО] \/${commands.enableAccounts} - перманентно возобновить участие аккаунтов в продвижении
  
  `,

  igAccountNotFound: `
Указанный аккаунт в базе не найден!

Проверьте правильность ввода аккаунта или свяжитесь с администраторами Беседки для решения проблем с подключением.

Для повторного ввода аккаунта IG используйте команду \/${commands.setIg}
  `
}
