module.exports = {
  setIg: 'setig',
  getMyTag: 'mytag',
  suspend: 'suspend',
  resume: 'resume',
  addAccounts: 'addAccounts',
  addTags: 'addTags',
  disableAccounts: 'disableAccounts',
  enableAccounts: 'enableAccounts',
  
}
