import commands from "./commands"

export default keyboard = [
  [
    {
      text: 'Указать акк. IG',
      callback_data: commands.setIg
    }
  ],
  [
    {
      text: 'Мои команды',
      callback_data: commands.getMyCommands
    }
  ],
  [
    {
      text: 'Мой тэг',
      callback_data: commands.getMyTag
    }
  ],
  [
    {
      text: 'Курсы Беседки',
      url: 'https://example.com' // TODO: указать ссылку на курсы беседки
    }
  ]
];