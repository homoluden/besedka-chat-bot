module.exports = {
  getAllIgAccounts,
  getIgAccountRef,
  getIgAccountByName,
  updateIgAccount
}

async function getAllIgAccounts(db) {
  try {
    const accountsRef = await db.ref(`igAccounts`).get();
    return accountsRef.val();
  } catch (error) {
    console.error(error);
  }
}

async function getIgAccountByName(db, igAccountName) {
  const igAccountSnapshot = await getIgAccountRef(db, igAccountName);
  return igAccountSnapshot.val();
}

async function getIgAccountRef(db, igAccountName) {
  try {
    return await db.ref(`igAccounts/${igAccountName}`).get();
  } catch (error) {
    console.error(error);
  }
}

async function updateIgAccount(db, igAccountData) {
  try {
    return await db.ref(`igAccounts/${igAccountData.userName}`).update(igAccountData);
  } catch (error) {
    console.error(error);
  }
}
