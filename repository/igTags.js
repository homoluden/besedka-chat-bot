module.exports = {
  setIgTags
}

async function setIgTags(db, newTags) {
  try {
    await db.ref(`igTags`).set(newTags);
  } catch (error) {
    console.error(error);
  }
}
