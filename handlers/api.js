module.exports = {
  startApi: () => {
    const express = require('express');
    const app = express()
    const port = process.env.PORT || 5000

    app.get('/favicon.ico', (req, res) => res.status(200))

    app.get('/', (req, res) => {
      res.send('Besedka Chatbot API!')
    })

    app.listen(port, () => {
      console.log(`Example app listening at http://localhost:${port}`)
    })
  }
}