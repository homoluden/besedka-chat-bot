const commands = require("../keyboard/commands");
const adminMessages = require("../messages/admin");
const { getIgAccountRef, updateIgAccount, getAllIgAccounts } = require("../repository/igAccount");
const { setIgTags } = require("../repository/igTags");
const { shuffle } = require("../helpers/randomHelper");

module.exports = {
  handleAddAccounts: async (bot, db) => {
    const addAccountsRegEx = new RegExp(`\/${commands.addAccounts}`);
    bot.onText(addAccountsRegEx, async (msg) => {
      const chatId = msg.chat.id;
    
      const addAccountsReply = await bot.sendMessage(chatId, adminMessages.inputAccountNames, {
        reply_markup: { force_reply: true }
      });
    
      bot.onReplyToMessage(addAccountsReply.chat.id, addAccountsReply.message_id, async (msg) => {
        const accountNames = [...msg.text.matchAll(/[@]{1}([\w\._]+)(?:[\s]+|$)/gm)].map(m => m[1]);
        let amountAdded = 0;

        for (let i = 0; i < accountNames.length; i++) {
          const accountName = accountNames[i];
          const escapedName = accountName.replace('.', '*');
          const igAccountRef = await getIgAccountRef(db, escapedName);
          if (igAccountRef?.exists()) {
            console.warn(`handleAddAccounts >> Account already exist: ${accountName}`);
          } else {
            await updateIgAccount(db, {
              userName: escapedName,
              isActive: true
            });
            amountAdded++;
          }
        }

        await bot.sendMessage(chatId, `Аккаунтов добавлено: ${amountAdded}`);
      });
    });
  },
  handleAddTags: async (bot, db) => {
    const addTagsRegEx = new RegExp(`\/${commands.addTags}`);
    bot.onText(addTagsRegEx, async (msg) => {
      const chatId = msg.chat.id;
    
      const addTagsReply = await bot.sendMessage(chatId, adminMessages.inputTags, {
        reply_markup: { force_reply: true }
      });
    
      bot.onReplyToMessage(addTagsReply.chat.id, addTagsReply.message_id, async (msg) => {
        const tagNames = [...msg.text.matchAll(/[#]{0,1}([\wа-яА-Я\._]+)[\s]*[,]*[\s]*/gm)].map(m => m[1]);

        setIgTags(db, tagNames);
        const allAccounts = await getAllIgAccounts(db);
        
        let accountNames = shuffle(Object.entries(allAccounts).map(e => e[0]));

        const accPerTag = Math.ceil(accountNames.length / tagNames.length);
        const accountUpdates = {};
        tagNames.forEach((t) => {
          let j = accPerTag;
          while(j > 0 && accountNames.length > 0) {
            j--;
            const accountName = accountNames.shift()
            updateIgAccount(db, {
              userName: accountName,
              currentTag: t
            })
          }
        });
      });
    });
  }
}