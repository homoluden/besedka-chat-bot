const commands = require("../keyboard/commands");
const startMsgs = require("../messages/start");
const { getIgAccountByName } = require("../repository/igAccount")

module.exports = {
  handleStart: async (bot, db) => {
    bot.onText(/\/start/, async (msg) => {
      const chatId = msg.chat.id;

      const chatSnapshot = await db.ref(`chats/${chatId}`).get();
      const chatData = chatSnapshot.val();

      const igAccountName = chatData?.igAccount;
      if (igAccountName) {
        const igAccountData = await getIgAccountByName(db, igAccountName);
        
        if (igAccountData) {
          bot.sendMessage(chatId, startMsgs.welcomeBack(igAccountName.replace('*', '.')) + getAvailableCommandsMsg(igAccountData.isAdmin));
        } else {
          await askToSetIg(chatId);
        }
      } else {
        await askToSetIg(chatId);
      }
      
    });
  },
  handleSetIg: async (bot, db) => {
    const setIgRegex = new RegExp(`\/${commands.setIg}`);
    bot.onText(setIgRegex, async (msg) => {
      const chatId = msg.chat.id;
    
      const setIgReply = await bot.sendMessage(chatId, startMsgs.setIg, {
        reply_markup: { force_reply: true }
      });
    
      bot.onReplyToMessage(setIgReply.chat.id, setIgReply.message_id, async (msg) => {
        const unescapedIgName = msg.text;
        const igAccountName = unescapedIgName.replace('.', '*');
    
        const igAccountData = await getIgAccountByName(db, igAccountName);
    
        if (igAccountData) {
          db.ref(`chats/${chatId}`).set({
            igAccount: igAccountName,
          });
          bot.sendMessage(chatId, `Аккаунт IG принят!\nВам доступны следующие команды:\n` + getAvailableCommandsMsg(igAccountData.isAdmin));
        } else {
          bot.sendMessage(chatId, startMsgs.igAccountNotFound);
        }
      });
    });
  },
  handleGetMyTag: async (bot, db) => {
    const getMyTagRegex = new RegExp(`\/${commands.getMyTag}`);
    bot.onText(getMyTagRegex, async (msg) => {
      const chatId = msg.chat.id;
    
      const chatSnapshot = await db.ref(`chats/${chatId}`).get();
      const chatData = chatSnapshot.val();

      const igAccountName = chatData?.igAccount;
      if (igAccountName) {
        const igAccountData = await getIgAccountByName(db, igAccountName);
        
        if (igAccountData) {
          bot.sendMessage(chatId, `Ваш текущий тег: #${igAccountData.currentTag}`);
        } else {
          await askToSetIg(chatId);
        }
      } else {
        await askToSetIg(chatId);
      }
      
    });
  }
}

async function askToSetIg(chatId) {
  await bot.sendMessage(chatId, startMsgs.welcomeFirst);
}

function getAvailableCommandsMsg(isAdmin) {
  return isAdmin ? (startMsgs.regularCommands + startMsgs.adminCommands) : startMsgs.regularCommands
}
