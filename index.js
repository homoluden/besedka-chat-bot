const tgToken = '1828655131:AAHfkvVbtEQ17GzfrHTRcqOgBDf1nxPuSj8';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBpSS7Kqdch5-onMgyh-XQkNF_vzfefVGc",
  authDomain: "besedka-chat-bot.firebaseapp.com",
  databaseURL: "https://besedka-chat-bot-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "besedka-chat-bot",
  storageBucket: "besedka-chat-bot.appspot.com",
  messagingSenderId: "18190952339",
  appId: "1:18190952339:web:ba16ea20b861b3a4b1aa66",
  measurementId: "G-XCR2RHHGJV"
};

const TelegramBot = require('node-telegram-bot-api');
const firebase = require('firebase');

const bot = new TelegramBot(tgToken, {polling: true});
exports.bot = bot;

firebase.initializeApp(firebaseConfig);

const db = firebase.database();
exports.db = db;

const { handleStart, handleSetIg, handleGetMyTag } = require("./handlers/userCommands");
handleStart(bot, db);
handleSetIg(bot, db);
handleGetMyTag(bot, db);

const { handleAddAccounts, handleAddTags } = require("./handlers/adminCommands");
handleAddAccounts(bot, db);
handleAddTags(bot, db);

const { startApi } = require("./handlers/api");
startApi();
